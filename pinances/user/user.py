from pinances import BaseDomainEntity

from pinances.common.validation import (
    Validation,
    validate_required_property,
)


class User(BaseDomainEntity):

    def __init__(self, email, name):
        self.email = email
        self.name = name

    def get_validations(self):
        return [
            Validation(validate_required_property, self.email, 'email'),
            Validation(validate_required_property, self.name, 'name'),
        ]


class NotValidUserError(Exception):

    def __init__(self, errors):
        super().__init__()
        self.errors = errors
