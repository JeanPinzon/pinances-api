import logging

from .user import User


logger = logging.getLogger('UserRepository')


class UserRepository():

    def __init__(self, mongo_client):
        self._mongo_client = mongo_client

    async def save(self, user):
        logger.info(f'Saving user with email={user.email}')

        await self._mongo_client.users.find_one_and_update(
            {'_id': user.email},
            {'$set': {
                'email': user.email,
                'name': user.name,
            }},
            upsert=True
        )

        logger.info(f'user with email={user.email} saved with success')

    async def get(self, email):
        logger.info(f'Getting user with email={email}')

        result = await self._mongo_client.users.find_one({'_id': email})

        if not result:
            logger.info(f'User with email={email} not found')
            return None

        logger.info(f'User with email={email} found. email={result.get("_id")}')

        return User(
            result.get('email'),
            result.get('name'),
        )
