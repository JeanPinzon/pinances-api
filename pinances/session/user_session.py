import hashlib
from pinances import BaseDomainEntity

from pinances.common.validation import (
    Validation,
    validate_required_property,
)


class UserSession(BaseDomainEntity):

    def __init__(self, user, google_id_token):
        self.user = user
        self.google_id_token = google_id_token

    @property
    def token(self):
        key = f'{self.user.email}{self.user.name}{self.google_id_token}'
        return hashlib.sha256(key.encode()).hexdigest()

    def get_validations(self):
        return [
            Validation(validate_required_property, self.google_id_token, 'googleIdToken'),
        ] + self.user.get_validations()


class NotValidUserSessionError(Exception):

    def __init__(self, errors):
        super().__init__()
        self.errors = errors
