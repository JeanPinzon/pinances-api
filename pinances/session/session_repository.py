import logging
import json

from .user_session import UserSession
from pinances.user.user import User


logger = logging.getLogger('SessionRepository')


class SessionRepository():

    def __init__(self, redis_client):
        self._redis_client = redis_client

    async def save(self, user_session):
        await self._redis_client.set(user_session.token, json.dumps({
            'name': user_session.user.name,
            'email': user_session.user.email,
        }))

    async def get(self, token):
        value = await self._redis_client.get(token)

        if not value:
            return None

        value_dict = json.loads(value)

        return UserSession(
            User(
                value_dict['email'],
                value_dict['name'],
            ),
            token,
        )

    async def delete(self, token):
        await self._redis_client.delete(token)
