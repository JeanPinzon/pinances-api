import os

LOG_LEVEL = int(os.environ.get('LOG_LEVEL', 10))
PORT = int(os.environ.get('PORT', 8000))
DEBUG = os.environ.get('DEBUG', False) == 'True'
MONGODB_URI = os.environ.get('MONGODB_URI', 'mongodb://localhost:27017/pinances')
MONGODB_DATABASE_NAME = os.environ.get('MONGODB_DATABASE_NAME', 'pinances')
REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost')
GOOGLE_TOKEN_INFO_URL = os.environ.get('GOOGLE_TOKEN_INFO_URL', 'https://oauth2.googleapis.com/tokeninfo')
GOOGLE_CLIENT_ID = os.environ.get('GOOGLE_CLIENT_ID', '519635575360-lhn80duauglmo19otquvtpqbsnqa2966.apps.googleusercontent.com')
GOOGLE_CLIENT_SECRET = os.environ.get('GOOGLE_CLIENT_SECRET', 'dBnicPE7R0qlwqwv7fNu45FM')

DEFAULT_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

TIMESTAMP_FORMAT_ERROR = f'Timestamp must have the format "{DEFAULT_DATETIME_FORMAT}"'
FLOAT_SMALLER_THAN_ONE_ERROR = 'The value must be bigger or equal to zero'
NON_FLOAT_ERROR = 'Value must be a float'
PROPERTY_REQUIRED = 'Value is required'

VALIDATE_NOT_CALLED_ERROR = (
    'You have to call the validate() method after to '
    'use the properties is_valid or validation_error'
)
