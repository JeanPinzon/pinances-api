import time
from datetime import datetime

from pinances.settings import (
    DEFAULT_DATETIME_FORMAT,
)


def string_to_datetime(timestamp_str):
    return datetime.strptime(timestamp_str, DEFAULT_DATETIME_FORMAT)


def datetime_to_str(datetime_instance):
    return datetime_instance.strftime(DEFAULT_DATETIME_FORMAT)
