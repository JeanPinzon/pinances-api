import re
from datetime import datetime, timedelta

from . import cast
from pinances.settings import (
    FLOAT_SMALLER_THAN_ONE_ERROR,
    NON_FLOAT_ERROR,
    TIMESTAMP_FORMAT_ERROR,
    VALIDATE_NOT_CALLED_ERROR,
    PROPERTY_REQUIRED,
)


class Validation():

    def __init__(self, validate_function, value, property):
        self.validate_function = validate_function
        self.value = value
        self.property = property
        self._validated = False

    def validate(self):
        self._validated = True
        self._is_valid, self._validation_error = self.validate_function(self.value)
        return self._is_valid

    @property
    def is_valid(self):
        if not self._validated:
            raise ValidateNotCalledError()

        return self._is_valid

    @property
    def validation_error(self):
        if not self._validated:
            raise ValidateNotCalledError()

        return self._validation_error


class ValidateNotCalledError(Exception):

    def __init__(self):
        super().__init__()
        self.errors = [VALIDATE_NOT_CALLED_ERROR]


def validate_timestamp(timestamp):
    try:
        if type(timestamp) == datetime:
            return True, None

        cast.string_to_datetime(timestamp)
        return True, None
    except Exception:
        return False, TIMESTAMP_FORMAT_ERROR


def validate_float_bigger_than_zero(value):
    try:
        value_float = float(value)

        if value_float <= 0:
            return False, FLOAT_SMALLER_THAN_ONE_ERROR

        return True, None
    except Exception:
        return False, NON_FLOAT_ERROR


def validate_required_property(value):
    if value is None:
        return False, PROPERTY_REQUIRED

    return True, None
