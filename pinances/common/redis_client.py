import aioredis


class RedisClient:

    def __init__(self, connection_string):
        self.connection_string = connection_string

    async def set(self, key, value):
        redis = await aioredis.create_redis_pool(self.connection_string)

        await redis.set(key, value)

        redis.close()
        await redis.wait_closed()

    async def get(self, key):
        redis = await aioredis.create_redis_pool(self.connection_string)

        value = await redis.get(key, encoding='utf-8')

        redis.close()
        await redis.wait_closed()

        return value

    async def delete(self, key):
        redis = await aioredis.create_redis_pool(self.connection_string)

        redis.delete(key)

        redis.close()
        await redis.wait_closed()
