import logging
import motor
import tornado.ioloop

from tornado.web import Application as TornadoApplication
from tornado.web import URLSpec

from pinances import settings
from pinances.common.redis_client import RedisClient

from pinances.api.handlers.auth_handler import AuthHandler
from pinances.api.handlers.expenses_handler import ExpenseHandler
from pinances.api.handlers.main_handler import MainHandler

from pinances.auth.auth_service import AuthService
from pinances.expenses.expenses_repository import ExpenseRepository
from pinances.expenses.expenses_service import ExpenseService
from pinances.session.session_repository import SessionRepository
from pinances.user.user_repository import UserRepository


logger = logging.getLogger('api')

mongo_client = motor.motor_tornado.MotorClient(settings.MONGODB_URI)[settings.MONGODB_DATABASE_NAME]
redis_client = RedisClient(settings.REDIS_URL)

expense_repository = ExpenseRepository(mongo_client)
user_repository = UserRepository(mongo_client)
session_repository = SessionRepository(redis_client)

expense_service = ExpenseService(expense_repository)
auth_service = AuthService(session_repository, user_repository)


class Application(TornadoApplication):

    def __init__(self, debug=False):
        super().__init__(self._routes(), debug=debug)

    def _routes(self):
        return [
            URLSpec(
                r'/api/v1/auth/(.*)?',
                AuthHandler,
                dict(auth_service=auth_service)
            ),
            URLSpec(
                r'/api/v1/expenses/(.*)?',
                ExpenseHandler,
                dict(expense_service=expense_service, auth_service=auth_service)
            ),
            URLSpec(
                r'/?',
                MainHandler,
                dict(auth_service=auth_service)
            ),
        ]


app = Application(debug=settings.DEBUG)


if __name__ == '__main__':
    logger.info(f'Running app using port {settings.PORT}')
    app.listen(settings.PORT)
    tornado.ioloop.IOLoop.current().start()
