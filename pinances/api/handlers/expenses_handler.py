import json
import logging

from pinances.api.handlers import BaseHandler, authenticated
from pinances.common.cast import datetime_to_str
from pinances.expenses.expense import (
    Expense,
    NotValidExpenseError
)


logger = logging.getLogger('ExpenseHandler')


class ExpenseHandler(BaseHandler):

    def initialize(self, expense_service, auth_service):
        super().initialize(auth_service)
        self.expense_service = expense_service

    @authenticated
    async def get(self, expense_id):
        if expense_id:
            logger.info(f'Getting expense with id={expense_id}')

            expense = await self.expense_service.get(expense_id)

            if not expense:
                self.set_status(404)
                return self.finish()

            result = self._expense_to_json(expense)
        else:
            logger.info('Listing expenses')

            expenses = await self.expense_service.list()
            result = json.dumps([self._expense_to_json(expense) for expense in expenses])

        self.set_status(200)
        self.finish(result)

    @authenticated
    async def post(self, expense_id):
        logger.info(f'Saving expense with id={expense_id}')

        try:
            expense = self._json_to_expense(self.request.body, expense_id)

            await self.expense_service.save(expense)

            logger.info(f'Expense with id={expense_id} saved with success')

            self.set_status(200)
            self.finish()
        except NotValidExpenseError as e:
            logger.exception(f'Failed to save expense with id={expense_id}', e)
            self.set_status(400)
            return self.finish(json.dumps(e.errors))

    def _json_to_expense(self, expense_json, expense_id):
        expense_dict = json.loads(expense_json)
        return Expense(
            expense_dict.get('id', expense_id),
            expense_dict.get('timestamp'),
            expense_dict.get('value'),
            expense_dict.get('description'),
        )

    def _expense_to_json(self, expense):
        return json.dumps({
            'id': expense.id,
            'timestamp': datetime_to_str(expense.timestamp),
            'value': expense.value,
            'description': expense.description,
        })
