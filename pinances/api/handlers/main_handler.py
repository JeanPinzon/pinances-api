from pinances.api.handlers import BaseHandler
from pinances import __version__ as version


class MainHandler(BaseHandler):

    def get(self):
        self.write(f'pinances_api - {version}')
