import json
import logging

from pinances.api.handlers import BaseHandler
from pinances.session.user_session import UserSession, NotValidUserSessionError
from pinances.user.user import User
from pinances.auth.auth_service import NotValidAuthTokenError


logger = logging.getLogger('AuthHandler')


class AuthHandler(BaseHandler):

    def initialize(self, auth_service):
        super().initialize(auth_service)
        self._auth_service = auth_service

    async def post(self, auth_token):
        logger.info('Authenticating user')

        try:
            user_session = self._json_to_user_session(self.request.body)

            token = await self._auth_service.authenticate(user_session)

            logger.info(f'User {user_session.user.email} authenticated with success')

            self.finish(json.dumps({'token': token}))
        except (NotValidUserSessionError, NotValidAuthTokenError) as e:
            logger.exception(f'Failed to authenticate user', e)
            self.set_status(400)
            return self.finish(json.dumps(e.errors))

    async def delete(self, auth_token):
        logger.info(f'Removing user session {auth_token}')

        await self._auth_service.remove_session(auth_token)

        logger.info(f'Session {auth_token} removed with success')

        self.set_status(200)
        self.finish()

    def _json_to_user_session(self, request_body):
        request_json = json.loads(request_body)
        return UserSession(
            User(
                request_json.get('email'),
                request_json.get('name'),
            ),
            request_json.get('googleIdToken'),
        )
