from tornado.web import RequestHandler, HTTPError


class BaseHandler(RequestHandler):

    def initialize(self, auth_service=None):
        self._auth_service = auth_service

    def set_default_headers(self, *args, **kwargs):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.set_header(
            'Access-Control-Allow-Methods', 
            'POST, GET, OPTIONS, DELETE, PUT'
        )

    def options(self, *args, **kwargs):
        self.set_status(204)
        self.finish()

    async def get_logged_user(self):
        token = self.request.headers.get('Authorization')

        if not token:
            return None

        logged_user = await self._auth_service.get_logged_user(token)

        return logged_user


def authenticated(f):
    async def wrapper(*args, **kwargs):
        handler_instance = args[0]

        logged_user = await handler_instance.get_logged_user()

        if not logged_user:
            raise HTTPError(403)

        return await f(*args, **kwargs)
    return wrapper
