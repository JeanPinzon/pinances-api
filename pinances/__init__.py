from pinances import settings
import logging
import sys


__author__ = 'Jean Pinzon'
__email__ = 'jean.pinzon1@gmail.com'
__version__ = '0.0.1'
__project__ = 'pinances'


logging.basicConfig(
    stream=sys.stdout,
    level=settings.LOG_LEVEL,
    format="level=%(levelname)s facility=%(name)s %(message)s"
)


class BaseDomainEntity():

    def is_valid(self):
        is_valid = True
        validation_errors = None

        for validation in self.get_validations():
            if not validation.validate():
                is_valid = False

                if validation_errors is None:
                    validation_errors = []

                validation_errors.append({
                    'property': validation.property,
                    'error': validation.validation_error
                })

        return is_valid, validation_errors

    def get_validations(self):
        raise NotImplementedError
