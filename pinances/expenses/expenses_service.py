import logging

from .expense import NotValidExpenseError


logger = logging.getLogger('ExpenseService')


class ExpenseService():

    def __init__(self, expense_repository):
        self._expense_repository = expense_repository

    async def save(self, expense):
        logger.info(f'Saving expense with id={expense.id}')

        is_expense_valid, validation_errors = expense.is_valid()

        if not is_expense_valid:
            raise NotValidExpenseError(validation_errors)

        await self._expense_repository.save(expense)

        logger.info(f'Expense with id={expense.id} saved with success')

    async def get(self, id):
        return await self._expense_repository.get(int(id))

    async def list(self):
        return await self._expense_repository.list()
