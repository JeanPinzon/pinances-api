import logging

from .expense import Expense


logger = logging.getLogger('ExpenseRepository')


class ExpenseRepository():

    def __init__(self, mongo_client):
        self._mongo_client = mongo_client

    async def save(self, expense):
        logger.info(f'Saving expense with id={expense.id}')

        await self._mongo_client.expenses.find_one_and_update(
            {'_id': expense.id},
            {'$set': {
                'value': expense.value,
                'timestamp': expense.timestamp,
                'description': expense.description,
            }},
            upsert=True
        )

        logger.info(f'expense with id={expense.id} saved with success')

    async def get(self, id):
        logger.info(f'Getting expense record with id={id}')

        result = await self._mongo_client.expenses.find_one({'_id': id})

        if not result:
            logger.info(f'Expense with id={id} not found')
            return None

        logger.info(f'Expense with id={id} found. id={result.get("_id")}')

        return Expense(
            result.get('_id'),
            result.get('timestamp'),
            result.get('value'),
            result.get('description'),
        )

    async def list(self):
        logger.info('Listing expenses')

        cursor = self._mongo_client.expenses.find({})

        records = []

        async for result in cursor:
            records.append(Expense(
                result.get('_id'),
                result.get('timestamp'),
                result.get('value'),
                result.get('description'),
            ))

        return records
