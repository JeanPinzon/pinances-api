from datetime import datetime

from pinances.common.validation import (
    Validation,
    validate_timestamp,
    validate_float_bigger_than_zero,
)

from pinances.common.cast import string_to_datetime


class Expense():

    def __init__(self, id, timestamp, value, description):
        self._id = id
        self._timestamp = timestamp
        self._value = value

        self.description = description

    @property
    def id(self):
        if self._id:
            return int(self._id)

        return None

    @property
    def value(self):
        return float(self._value)

    @property
    def timestamp(self):
        if type(self._timestamp) == datetime:
            return self._timestamp

        return string_to_datetime(self._timestamp)

    def is_valid(self):
        is_valid = True
        validation_errors = None

        for validation in self.get_validations():
            if not validation.validate():
                is_valid = False

                if validation_errors is None:
                    validation_errors = []

                validation_errors.append({
                    'property': validation.property,
                    'error': validation.validation_error
                })

        return is_valid, validation_errors

    def get_validations(self):
        return [
            Validation(validate_timestamp, self._timestamp, 'timestamp'),
            Validation(validate_float_bigger_than_zero, self._value, 'value'),
        ]


class NotValidExpenseError(Exception):

    def __init__(self, errors):
        super().__init__()
        self.errors = errors
