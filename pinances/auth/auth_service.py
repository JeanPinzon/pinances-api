import logging
import json

from tornado.httpclient import AsyncHTTPClient

from pinances.session.user_session import NotValidUserSessionError
from pinances.settings import (
    GOOGLE_TOKEN_INFO_URL,
    GOOGLE_CLIENT_ID
)


logger = logging.getLogger('AuthService')


class AuthService():

    def __init__(self, session_repository, user_repository):
        self._session_repository = session_repository
        self._user_repository = user_repository
        self._http_client = AsyncHTTPClient()

    async def authenticate(self, user_session):
        is_user_session_valid, validation_errors = user_session.is_valid()

        if not is_user_session_valid:
            raise NotValidUserSessionError(validation_errors)

        is_valid = await self._validate_google_token(user_session.google_id_token)

        if not is_valid:
            raise NotValidAuthTokenError()

        await self._user_repository.save(user_session.user)
        await self._session_repository.save(user_session)

        return user_session.token

    async def remove_session(self, auth_token):
        await self._session_repository.delete(auth_token)

    async def get_logged_user(self, auth_token):
        return await self._session_repository.get(auth_token)    

    async def _validate_google_token(self, google_token):
        logger.info('Validating google token')

        try:
            url = f'{GOOGLE_TOKEN_INFO_URL}?id_token={google_token}'

            response = await self._http_client.fetch(url)
            response_json = json.loads(response.body)

            is_token_valid = response_json.get('aud') == GOOGLE_CLIENT_ID

            logger.info(f'Google token validated. Valid={is_token_valid}')

            return is_token_valid
        except Exception as e:
            logger.exception('Failed to validate google token', e)
            return False


class NotValidAuthTokenError(Exception):

    def __init__(self):
        super().__init__()
        self.errors = [{
            'property': 'googleIdToken',
            'error': 'Access token not valid'
        }]
