FROM python:3.6


RUN apt-get update

WORKDIR /src
COPY ./ /src/
RUN pip install -r requirements_dev.txt
RUN pip install -r requirements.txt

EXPOSE 8000
