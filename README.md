# Pinances API

## Development environment

This application was created using a macOS Majove 10.14.5, but you just need docker to run all the environment.

If you don't have docker installed, you will need this requirements:

- Python 3.6
- MongoDB 4.0

> The python application requirements are configured into `requirements.txt` and `requirements_dev.txt`

### How to Setup

> If you use docker, it is not necessary

- `make setup`


### How to Run

#### Using docker

- `make docker-run`


#### Using local environment

- `make run`
