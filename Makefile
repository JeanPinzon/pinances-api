install: ## Install prod dependencies
	@pip install -r requirements.txt

setup: install ## Install dev dependencies
	@pip install -r requirements_dev.txt

run: ## Run application
	@PYTHONDONTWRITEBYTECODE=1 PYTHONPATH='.'  \
	python pinances/api/app.py

docker-run: ## Run application using docker
	@docker-compose up

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

help:
	@echo "\n#########################################################################"
	@echo "################################## HELP #################################"
	@echo "#########################################################################\n"

	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_\-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

	@echo "\n#########################################################################\n"

.DEFAULT_GOAL := help

.PHONY: install, setup, run, test-unit, test-code-quality, docker-run, docker-test-unit, docker-test-code-quality, help
